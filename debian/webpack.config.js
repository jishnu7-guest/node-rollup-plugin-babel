'use strict';
var path = require('path');
var config = {
  resolve: {
    modules: ['/usr/lib/nodejs', 'node_modules'],
  },
  resolveLoader: {
    modules: ['/usr/lib/nodejs'],
  },
  module: { rules: [ { use: [ 'babel-loader' ] } ] }
}
module.exports = config;
